import inviter
import traceback

if __name__ == '__main__':
    try:
        inviter.run()
    except Exception as e:
        print(e)
        print('Traceback: ')
        traceback.print_exc()
