# pylint: disable=C0103
# pylint: disable=C0111

"""
users orm
"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Boolean, Integer,MetaData


Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True, unique=True)
    is_in_friend_list = Column(Boolean)
    is_notified = Column(Boolean)
    bot_id = Column(Integer)

    def __init__(self, uid, is_in_friend_list, is_notified, bot_id):
        self.uid = uid
        self.is_in_friend_list = is_in_friend_list
        self.is_notified = is_notified
        self.bot_id = bot_id

    def __repr__(self):
        return "<User('id%s','%s','%s')>" % (self.uid, self.is_in_friend_list,self.bot_id)
