from ..models.users import User
import time
import sys

class Manager(object):
    def __init__(self, db_session_class, bots):
        self.session = db_session_class()
        self.bots = bots

    def process(self):
        for bot in [b for b in self.bots if b.ready()]:  # only ready bots
            if self.session.query(User).filter_by(bot_id=bot.uid).count() > 0:
                return  # users for this bot alreay on db

            time.sleep(1)
            #print(bot.api.users.search(
            #    city=bot.city,
            #    sort=1,
            #    count=1000))
            print(f"GroupID: {bot.group_id}")
            search_results = bot.api.users.search(
                group_id=bot.group_id,
                city=bot.city,
                sort=1,
                count=1000)["items"][1:]  # first element is results cuont
            print(search_results)
            print(
                u"bot %s (%s) is searching new users" % (bot.uid, bot.city))
            for user in search_results:
                self.session.merge(User(  # using merge for 'insert or ignore'
                    user['id'], False, False, False, False, int(bot.uid)))

            self.session.commit()
