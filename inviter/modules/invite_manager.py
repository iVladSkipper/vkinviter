from ..models.users import User
import time
from vk_api.exceptions import ApiError
from vk_api.exceptions import Captcha
from vk_api.exceptions import AccessDenied
import datetime;
import pprint

class Manager(object):
    def __init__(self, db_sessuib_class, bots, group_id):
        self.session = db_sessuib_class()
        self.bots = bots
        self.group_id = group_id

    def process(self):
        for bot in [b for b in self.bots if b.ready()]:  # only ready bots
            try:
                print("try _invite_users")
                self._invite_users(bot)
            except Captcha as e:
                print(f"Captcha(process): {e} {e.get_url()}")
                bot.sleep(3)
                continue
            except ApiError as e:
                print(f"ApiError(process): {e}")
                bot.sleep(3)
                continue

    def _invite_users(self, bot):
        users = self.session.query(User).filter_by(bot_id=bot.uid) \
            .filter_by(is_in_friend_list=True) \
            .filter_by(is_group_invite_sent=False) \
            .filter_by(is_invite_access_denied=False)

        for user in users:
            time.sleep(5)
            self._invite_specific_user(user, bot)

        self.session.commit()

    def _invite_specific_user(self, u, bot):
        print(f"try invite {u.uid}")
        is_member = bot.api.groups.isMember(
            group_id=self.group_id, user_id=u.uid)
        is_invite_access_denied = u.is_invite_access_denied
        if not is_member and not is_invite_access_denied:
            # if user not a member then (re)send group invite
            print('inviting user %s to group' % u.uid)
            try:
                bot.api.groups.invite(group_id=self.group_id, user_id=u.uid)
            except AccessDenied as e:
                u.is_invite_access_denied = True
                print(f"AccessDenied: {e}")
                pass
            except ApiError as e:
                print("Gotcha!")
                pprint.pprint(e.error['error_msg'])
                if e.error['error_msg'] == "Access denied: already sent" or e.error['error_msg'] == 'Access denied: could invite only friends' or e.error['error_msg'] == 'Access denied: could invite only friends':
                    print("Is Equal")
                    u.is_invite_access_denied = True
                print(f"ApiError: {e}")
                pass
            except Exception as e:
                print(e)
                raise
        # even if already in group set property to True
        # to prevent further invites
        u.grpinvite_date = datetime.datetime.now()
        u.is_group_invite_sent = True
