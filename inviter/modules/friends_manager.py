from ..models.users import User
import time
from vk_api.exceptions import ApiError
from vk_api.exceptions import Captcha
from datetime import datetime
from datetime import timedelta
from sqlalchemy import *

class Manager(object):
    def __init__(self, session_class, bots):
        self.session = session_class()
        self.bots = bots

    def process(self):
        for bot in [b for b in self.bots if b.ready()]:  # only ready bots
            try:
                print("try _send_friend_request")
                self._send_friend_request(bot)
                print("try _check_friends")
                self._check_friends(bot)
            except Captcha as e:
                print(f"Captcha: {e.get_url()}")
                bot.sleep(3)
                continue
            except ApiError as e:
                if e.error["error_msg"] == 'Unknown error occurred':
                    print('Limit send_frend_request access')
                    pass
                raise

    def _send_friend_request(self, bot):
        users = self.session.query(User).filter_by(bot_id=bot.uid) \
            .filter_by(is_friend_request_sent=False) \
            .filter_by(is_in_friend_list=False)

        for u in users[:2]:
            are_friends = bot.api.friends.areFriends(user_ids=u.uid)

            if are_friends[0]['friend_status'] == 3:  # are friends
                u.is_in_friend_list = True
                print('user %s already in friendlist' % u.uid)
                continue
            if are_friends[0]['friend_status'] == 1:  # you subscribed
                u.is_friend_request_sent = True
                print(
                    'friend reuest already sent to user %s' % u.uid)
                continue

            print('send friends request to %s' % u.uid)
            bot.api.friends.add(user_id=u.uid)
            u.friendreq_date = datetime.now()
            u.is_friend_request_sent = True
            self.session.commit()
            time.sleep(5)

        self.session.commit()

    def _check_friends(self, bot):
        tdelta = datetime.now() - timedelta(hours=1)
        
        users = (self.session.query(User).filter(or_(\
                        User.chkfriend_date == None,\
                        User.chkfriend_date < tdelta))\
            .filter_by(bot_id=bot.uid) \
            .filter_by(is_friend_request_sent=True) \
            .filter_by(is_in_friend_list=False))

        for u in users:
            #if(datetime.datetime.now() - u.chkfriend_date).hour > 1:
            #    print(f"Skip user with uid: {u.uid}")
            #    continue 
            time.sleep(5)
            are_friends = bot.api.friends.areFriends(user_ids=u.uid)

            if u.chkcycles == None:
                u.chkcycles = 1;
            else:
                u.chkcycles = u.chkcycles + 1;
            u.chkfriend_date = datetime.now()
            print('check friend status for %s' % are_friends[0])
            if are_friends[0]['friend_status'] == 3:  # are friends
                u.is_in_friend_list = True
                print('user %s accepst friend request' % u.uid)
            self.session.commit()
        self.session.commit()
