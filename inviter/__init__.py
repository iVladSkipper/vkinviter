from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .models.users import Base as users_base
from .models.bot import Bot
from .modules.friends_manager import Manager as FriendsManager
from .modules.invite_manager import Manager as InviteManager
from .modules.database_manager import Manager as DatabaseManager
import json
import time
from random import randint
from time import sleep

def run():
    engine = create_engine('sqlite:///database.sqlite3', echo=False)
    users_base.metadata.create_all(engine)
    session_class = sessionmaker(bind=engine)

    with open('config.json') as data_file:
        config = json.load(data_file)
        bots = Bot.from_dict(config)

    fm = FriendsManager(session_class, bots)
    im = InviteManager(session_class, bots, config['invite_group_id'])
    dbm = DatabaseManager(session_class, bots)

    dbm.process()
    while True:
        time.sleep(1)
        im.process()
        fm.process()
        sleep(randint(5,20))

if __name__ == '__main__':
    run()
