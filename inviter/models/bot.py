#import vk
import time
import traceback
import vk_api
from vk_api import exceptions
from pprint import pprint
def two_factor_handler():
    key = input('Введите код двух-факторной авторизации: ?')
        # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device

def captcha_handler(captcha):
    """ При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
    """
    pprint(captcha)

    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

    # Пробуем снова отправить запрос с капчей
    return captcha.try_again(key)


class Bot(object):
    _sleepunil = 0

    def __init__(self, vk_session, city: str, groupid: int):
        
        try:
            vk_session.auth()
        except vk_api.AuthError as error_msg:
            print(error_msg)
            traceback.print_exc()
            pass
        
        self.search_offset = 0
        self.search_chunk_size = 1000
        self.group_invite_offset = 0
        self.friend_request_offset = 0
        
        self.access_token = vk_session._auth_token()
        print(self.access_token)
        self.api = vk_session.get_api()
        self.group_id = groupid
        pprint(self.api.users.get())

        self.uid = self.api.users.get()[0]['id']
        cities = self.api.database.getCities(
            country_id=1, count=1, q=city)

        self.city = cities["items"][0]["id"]
        self.city_str = cities["items"][0]["title"]

    @classmethod
    def from_dict(cls, dic: dict):
        res = []
        for b in dic['bots']:
            if 'token' in b:
                res.append(cls(
                    vk_api.VkApi(token=b['token']),
                    b['city'], dic['group_id']))
                continue
            cityStr = f"City: {b['city']}"
            print(cityStr)
            res.append(cls(vk_api.VkApi(login=b['login'],
                password=b['password'], captcha_handler=captcha_handler, auth_handler=two_factor_handler, scope='offline,friends,groups'), b['city'], dic['group_id']))

        return res

    def ready(self):
        return not self._is_sleeping()

    def _is_sleeping(self):
        return self._sleepunil > time.time()

    def sleep(self, minutes):
        self._sleepunil = time.time() + (60 * minutes)
