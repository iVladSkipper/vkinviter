from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Boolean, Integer, DateTime


Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True, unique=True)
    is_friend_request_sent = Column(Boolean)
    is_in_friend_list = Column(Boolean)
    is_group_invite_sent = Column(Boolean)
    is_invite_access_denied = Column(Boolean)
    bot_id = Column(Integer)

    #stat
    searchdate = Column(DateTime)
    chkfriend_date = Column(DateTime)
    chkcycles = Column(Integer)
    friendreq_date = Column(DateTime)    
    grpinvite_date = Column(DateTime)

    def __init__(
            self, uid, is_in_friend_list,
            is_friend_request_sent, is_group_invite_sent, is_invite_access_denied, bot_id):

        self.uid = uid
        self.is_in_friend_list = is_in_friend_list
        self.is_friend_request_sent = is_friend_request_sent
        self.is_group_invite_sent = is_group_invite_sent
        self.is_invite_access_denied = is_invite_access_denied
        self.bot_id = bot_id

    def __repr__(self):
        return "<User(id%s)>" % (self.uid,)
